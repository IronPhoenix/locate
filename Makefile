CXX=g++
FLAGS=-std=c++11 -g -Wall
LDFLAGS=-lboost_program_options -lboost_filesystem -lboost_system -lboost_serialization
SOURCE_DIR=src
BUILD_DIR=obj
APPS=locate updatedb
HEADER_NAMES=file.h suffix.h
OBJECT_NAMES=file_indexer.o  file_system_walker.o sort_task.o
HEADERS=$(patsubst %, $(SOURCE_DIR)/%, $(HEADER_NAMES))
OBJECTS=$(patsubst %, $(BUILD_DIR)/%, $(OBJECT_NAMES))

.PHONY: all
all: $(APPS)

locate: $(BUILD_DIR)/locate.o $(OBJECTS) 
	g++ $^ -o $@ $(FLAGS) $(LDFLAGS)

updatedb: $(BUILD_DIR)/updatedb.o $(OBJECTS) 
	g++ $^ -o $@ $(FLAGS) $(LDFLAGS)

$(BUILD_DIR)/locate.o: $(SOURCE_DIR)/locate.cpp $(HEADERS) | $(BUILD_DIR)
	g++ $(FLAGS) -c $< -o $@

$(BUILD_DIR)/updatedb.o: $(SOURCE_DIR)/updatedb.cpp $(HEADERS) | $(BUILD_DIR)
	g++ $(FLAGS) -c $< -o $@

$(BUILD_DIR)/%.o: $(SOURCE_DIR)/%.cpp $(SOURCE_DIR)/%.h $(HEADERS) | $(BUILD_DIR)
	g++ $(FLAGS) -c $< -o $@

$(BUILD_DIR):
	mkdir $(BUILD_DIR)

.PHONY: clean
clean:
	rm $(BUILD_DIR) -Rf
	rm $(APPS) -f
