#include "sort_task.h"

#include <climits>
#include <vector>

using std::vector;
using std::move;
using std::make_pair;

void sort_task::sort_by_other_chars() {
    int index = range.first;
    char index_char = get_char(range.first);
    for(int i = range.first + 1; i != range.second; ++i) {
        char current = get_char(i);
        if(current != index_char) {
            if(index_char != 0) sort_range(index, i);
            index_char = current;
            index = i;
        }
    }
    if(index_char != 0) sort_range(index, range.second);
}

void sort_task::sort_by_char() {
    vector<suffix> sorted(range.second - range.first);
    int count[UCHAR_MAX + 1] = {};
    for(int i = range.first; i != range.second; ++i)
        ++count[get_char(i)];
    for(size_t i = 1; i != UCHAR_MAX + 1; ++i)
        count[i] += count[i - 1];
    for(int i = range.second - 1; i >= range.first; --i)
        sorted[--count[get_char(i)]] = index.get_suffix_by_index(i);
    index.change_suffixes(sorted, range.first);
}

void sort_task::sort_range(int begin, int end) {
    if(end - begin > 1) service.dispatch(move(sort_task(shift + 1, make_pair(begin, end), index, service)));
}

unsigned char sort_task::get_char(suffix const & entry) const {
    size_t id = entry.get_suffix_id() + shift;

    file const & f = index.get_file_by_index(entry.get_file_id());

    if(id >= f.get_name().size()) return 0;
    else return f.get_name()[id];
}

unsigned char sort_task::get_char(int id) const {
    return get_char(index.get_suffix_by_index(id));
}
