#ifndef SUFFIX_H
#define SUFFIX_H

#include <boost/serialization/access.hpp>

struct suffix {
    suffix() {}

    suffix(int file_id, int suffix_id):
        file_id(file_id),
        suffix_id(suffix_id)
    {}

    int const & get_file_id() const {
        return file_id;
    }

    int const & get_suffix_id() const {
        return suffix_id;
    }

private:
    int file_id;
    int suffix_id;

    friend class boost::serialization::access;
    template<class Archive>
    void serialize(Archive & ar, const unsigned int version) {
        ar & file_id;
        ar & suffix_id;
    }
};

#endif // SUFFIX_H
