#ifndef SORT_TASK_H
#define SORT_TASK_H

#include "suffix.h"
#include "file_indexer.h"

#include <boost/asio.hpp>

using std::pair;

using boost::asio::io_service;

struct sort_task{
    sort_task(int shift, pair<int, int> range, file_indexer & index, io_service & service):
        shift(shift),
        range(range),
        index(index),
        service(service)
    {}

    void operator()() {
        sort_by_char();
        sort_by_other_chars();
    }

private:
    void sort_by_other_chars();
    void sort_by_char();
    void sort_range(int begin, int end);
    unsigned char get_char(int id) const;
    unsigned char get_char(const suffix &entry) const;

    int shift;
    pair<int, int> range;
    file_indexer & index;
    io_service & service;
};

#endif // SORT_TASK_H
