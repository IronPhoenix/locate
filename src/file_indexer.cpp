#include "file_indexer.h"
#include "sort_task.h"

#include <utility>
#include <memory>
#include <algorithm>
#include <thread>
#include <set>
#include <climits>
#include <algorithm>

#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/iterator/counting_iterator.hpp>

using std::vector;
using std::thread;
using std::unique_ptr;
using std::move;
using std::make_pair;
using std::pair;
using std::set;
using std::thread;
using std::lower_bound;

using boost::bind;
using boost::asio::io_service;
using boost::counting_iterator;

file_indexer::file_indexer(vector<file> & files):
    threads_count((thread::hardware_concurrency() != 0) ? thread::hardware_concurrency() : 4),
    files(files)

{
    for(auto f: files)
        for(size_t i = 0; i != f.get_name().size(); ++i)
            suffixes.push_back(suffix(f.get_id(), i));
    sort_suffixes();
}

pair<int, int> file_indexer::find_range(string const & pattern) {
    auto comparator = [=] (int first, string s) -> bool { return get_suffix(first) < s; };
    counting_iterator<int> begin = lower_bound(counting_iterator<int>(0), counting_iterator<int>(suffixes.size() - 1), pattern, comparator);
    counting_iterator<int> end = lower_bound(counting_iterator<int>(0), counting_iterator<int>(suffixes.size() - 1), pattern + static_cast<char>(UCHAR_MAX), comparator);
    return make_pair(*begin, *end);
}

vector<file> file_indexer::find_files(string const & pattern) {
    pair<int, int> range = find_range(pattern);
    set<int> file_ids;
    for(int i = range.first; i < range.second; ++i)
        file_ids.insert(suffixes[i].get_file_id());
    vector<file> res;
    for(int id: file_ids)
        res.push_back(files[id]);
    return res;
}

void file_indexer::sort_suffixes() {
    io_service service;
    vector<thread> threads(threads_count);
    unique_ptr<io_service::work> work(new io_service::work(service));
    for(auto & t: threads)
        t = thread(bind(&io_service::run, &service));
    service.dispatch(move(sort_task(0, make_pair(0, suffixes.size()), *this, service)));
    work.reset();
    for(auto & thread: threads)
        thread.join();
}

//int file_indexer::lower_bound(string const & pattern) {
//    int begin = 0;
//    int end = static_cast<int>(suffixes.size() - 1);
//    while(end - begin > 0) {
//        int mid = (begin + end) / 2;
//        if(get_suffix(mid) < pattern) begin = mid + 1;
//        else end = mid;
//    }
//    if(get_suffix(end) < pattern) return suffixes.size();
//    else return end;
//}

string file_indexer::get_suffix(int id) {
    suffix & cur = suffixes[id];
    return files[cur.get_file_id()].get_name().substr(cur.get_suffix_id());
}
