#ifndef FILE_INDEXER_H
#define FILE_INDEXER_H

#include "suffix.h"
#include "file.h"

#include <vector>
#include <string>

#include <boost/serialization/access.hpp>
#include <boost/serialization/vector.hpp>

using std::vector;
using std::string;
using std::pair;
using std::copy;

struct file_indexer{
    file_indexer() {}
    explicit file_indexer(vector<file> & files);

    pair<int, int> find_range(string const & pattern);
    vector<file> find_files(string const & pattern);

    void change_suffixes(vector<suffix> const & suffixes, int begin) {
        copy(suffixes.begin(), suffixes.end(), this->suffixes.begin() + begin);
    }

    file const & get_file_by_index(int index) const {
        return files[index];
    }

    suffix const & get_suffix_by_index(int index) const {
        return suffixes[index];
    }

private:
    void sort_suffixes();
    //int lower_bound(string const & pattern);
    string get_suffix(int id);

    friend class boost::serialization::access;
    template<class Archive>
    void serialize(Archive & ar, const unsigned int version) {
        ar & files;
        ar & suffixes;
    }

    size_t threads_count;
    vector<file> files;
    vector<suffix> suffixes;
};

#endif // FILE_INDEXER_H
