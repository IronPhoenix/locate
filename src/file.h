#ifndef FILE_H
#define FILE_H

#include <string>

#include <boost/serialization/access.hpp>
#include <boost/serialization/string.hpp>

using std::string;

struct file {
    file() {}
    file(int id, string const & name, string const & path):
        id(id),
        name(name),
        path(path)
    {}

    int const & get_id() const {
        return id;
    }

    string const & get_name() const {
        return name;
    }

    string const & get_path() const {
        return path;
    }

private:
    int id;
    string name;
    string path;

    friend class boost::serialization::access;
    template<class Archive>
    void serialize(Archive & ar, const unsigned int version) {
        ar & id;
        ar & name;
        ar & path;
    }
};

#endif // FILE_H
