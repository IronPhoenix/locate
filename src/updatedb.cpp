#include "file.h"
#include "file_indexer.h"
#include "file_system_walker.h"

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <queue>
#include <utility>
#include <functional>
#include <iterator>

#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include <boost/archive/text_oarchive.hpp>

using std::ostream;
using std::cout;
using std::endl;
using std::cerr;
using std::ofstream;
using std::string;
using std::vector;
using std::exception;
using std::ios;

namespace po = boost::program_options;

using po::options_description;
using po::value;
using po::variables_map;
using po::store;
using po::parse_command_line;
using po::notify;

using boost::filesystem::path;
using boost::filesystem::exists;
using boost::filesystem::is_directory;

int main(const int argc, const char *argv[]) {
    const string help_message = "Usage: updatedb [--database-root DIR] [--output FILE] | --help";
    options_description desc("Allowed options");
    desc.add_options()
            ("help,h", "Show help message")
            ("database-root,r", value<string>()->required()->default_value("."), "Root catalog for scanning")
            ("output,o", value<string>()->required()->default_value("index.db"),"File to save index to");
    variables_map vm;
    try {
        store(parse_command_line(argc, argv, desc), vm);
        if (vm.count("help")) {
            cout << help_message << endl << desc << endl;
            return 0;
        }
        notify(vm);
    } catch (const exception & e) {
        cout << e.what() << endl;
        cout << help_message << endl << desc << endl;
        return 1;
    }
    string output_file_path = vm["output"].as<string>();
    ofstream output_file(output_file_path, ios::trunc);
    if (!output_file) {
        cerr << "Cannot open \"" << output_file_path << "\" for writing" << endl;
        return 2;
    }
    path root(vm["database-root"].as<string>());
    if (!exists(root)) {
        cerr << root << " doesn't exist" << endl;
        return 3;
    } else if (!is_directory(root)) {
        cerr << root << " is not a directory" << endl;
        return 4;
    }
    file_system_walker walker;
    vector<file> files = walker.traverse_directory(root.string());
    file_indexer index(files);
    boost::archive::text_oarchive output(output_file);
    output << index;
    return 0;
}
