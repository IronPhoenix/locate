#include "file_system_walker.h"

#include <thread>
#include <mutex>
#include <condition_variable>
#include <iostream>
#include <functional>
#include <stdexcept>

using std::cerr;
using std::endl;
using std::vector;
using std::unique_lock;
using std::mutex;
using std::thread;
using std::string;
using std::bind;

using boost::filesystem::path;
using boost::filesystem::canonical;
using boost::filesystem::is_symlink;
using boost::filesystem::is_directory;
using boost::filesystem::filesystem_error;
using boost::filesystem::directory_iterator;
using boost::system::error_code;

vector<file> file_system_walker::traverse_directory(string const & start) {
    threads_count = (thread::hardware_concurrency() != 0) ? thread::hardware_concurrency() : 4;
    files = vector< vector<path> >(threads_count);
    add_paths(start);
    return get_files();
}

bool file_system_walker::process_file(path file, size_t id) {
    bool is_dir = false;
    try {
        if (is_symlink(file)) return is_dir;
        path current = canonical(file);
        if(!is_symlink(file) && is_directory(file)) is_dir = true;
        files[id].push_back(current);
    }
    catch(filesystem_error const & ex) {
        cerr << "Error: " << file << ". " << ex.what() << endl;
        is_dir = false;
    }
    return is_dir;
}

void file_system_walker::process_dir(path dir, size_t id) {
    vector<path> new_dirs;
    error_code error;
    for(auto it = directory_iterator(dir, error); !error && it != directory_iterator(); it.increment(error)) {
        path current = it->path();
        if(process_file(current, id)) new_dirs.push_back(current);
    }
    if(error) {
        cerr << "Error: " << dir << ". " << error.message() << endl;
        return;
    }
    add_dirs_to_queue(new_dirs);
}

void file_system_walker::add_dirs_to_queue(vector<path> const & new_dirs) {
    unique_lock<mutex> ulock(queue_mutex);
    for(path dir: new_dirs)
        files_queue.push(dir);
    cond_var.notify_all();
}

void file_system_walker::add_paths(path start) {
    clear_all();
    if(process_file(start, 0))
        add_dirs_to_queue({start});
    else {
        cerr << "Error: " << start.string() << " is not a directory" << endl;
        return;
    }
    vector<thread> threads(threads_count);
    for(size_t i = 0; i != threads_count; ++i)
        threads[i] = thread(bind(&file_system_walker::traverse_task, this, i));
    for(size_t i = 0; i != threads_count; ++i)
        threads[i].join();
}

void file_system_walker::traverse_task(size_t id) {
    while(!done) {
        unique_lock<mutex> ulock(queue_mutex);
        while(!done && files_queue.empty()) {
            ++waiting_count;
            if(waiting_count == threads_count) break;
            cond_var.wait(ulock);
            --waiting_count;
        }
        if(files_queue.empty()) {
            done = true;
            cond_var.notify_all();
            break;
        }
        path current = files_queue.front();
        files_queue.pop();
        ulock.unlock();
        process_dir(current, id);
    }
}

void file_system_walker::clear_all() {
    for(size_t i = 0; i != threads_count; ++i)
        files[i].clear();
    while(!files_queue.empty())
        files_queue.pop();
    done = false;
    waiting_count = 0;
}

vector<file> file_system_walker::get_files() {
    vector<file> res;
    int id = 0;
    for(size_t i = 0; i != threads_count; ++i) {
        for(auto p: files[i]) {
            file f(id++, p.filename().string(), p.string());
            res.push_back(f);
        }
    }
    return res;
}
