#ifndef FILE_SYSTEM_WALKER_H
#define FILE_SYSTEM_WALKER_H

#include "file_indexer.h"

#include <queue>
#include <vector>
#include <string>
#include <mutex>
#include <condition_variable>

#include <boost/filesystem.hpp>

using std::vector;
using std::string;
using std::mutex;
using std::condition_variable;
using std::queue;

using boost::filesystem::path;

struct file_system_walker {
    vector<file> traverse_directory(string const & start);

private:
    bool process_file(path file, size_t id);
    void process_dir(path dir, size_t id);
    void add_dirs_to_queue(vector<path> const & new_dirs);
    void add_paths(path start);
    void traverse_task(size_t id);
    void clear_all();
    vector<file> get_files();

    size_t threads_count;

    bool done;
    size_t waiting_count;
    mutex queue_mutex;
    mutex err_mutex;
    condition_variable cond_var;

    queue<path> files_queue;
    vector< vector<path> > files;

};

#endif // FILE_SYSTEM_WALKER_H
