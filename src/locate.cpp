#include "file.h"
#include "file_indexer.h"
#include "file_system_walker.h"

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <set>

#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include <boost/archive/text_iarchive.hpp>

using std::ifstream;
using std::cout;
using std::endl;
using std::cerr;
using std::ofstream;
using std::string;
using std::vector;
using std::set;
using std::exception;

namespace po = boost::program_options;

using po::options_description;
using po::positional_options_description;
using po::value;
using po::variables_map;
using po::store;
using po::command_line_parser;
using po::notify;

using boost::system::error_code;
using boost::filesystem::exists;

int main(const int argc, const char* argv[]) {
    const string help_message = "Usage: locate [--database FILE] [-p] PATTERN | --help";
    options_description desc("Allowed options");
    desc.add_options()
            ("help,h", "Show help message")
            ("database,d", value<string>()->required()->default_value("index.db"), "Database file")
            ("pattern,p", value<string>()->required(), "Search pattern");
    positional_options_description pdesc;
    pdesc.add("pattern", 1);

    variables_map vm;

    try {
        store(command_line_parser(argc, argv).options(desc).positional(pdesc).run(), vm);
        if (vm.count("help")) {
            cout << help_message << endl << desc << endl;
            return 0;
        }
        notify(vm);
    } catch (exception const & e) {
        cout << e.what() << endl;
        cout << help_message << endl << desc << endl;
        return 1;
    }

    const string input_file_path = vm["database"].as<string>();
    const string pattern = vm["pattern"].as<string>();
    ifstream input_file(input_file_path);
    if (!input_file) {
        cerr << "Cannot open \""  << input_file_path << "\" for reading" << endl;
        return 2;
    }
    boost::archive::text_iarchive ia(input_file);
    file_indexer index;
    ia >> index;
    vector<file> files = index.find_files(pattern);
    for(file f: files) {
        error_code err;
        if(exists(f.get_path(), err) && !err) {
            cout << f.get_path() << endl;
        }
    }
    return 0;
}
